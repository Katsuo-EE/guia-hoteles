module.exports = function(grunt){

    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: './css',
                    src:  ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },

        watch: {
            files: ['css/*.scss'],
            tasks: ['css']
        },

        browserSync: {
            dev: {
                bsFiles: { //browser files
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './' // Directrio base apra nuestro servidor
                    }
                }
            }
        },
        
        imagemin:{
            dynamic: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    cwd: './img',
                    src: '**/*.{jpg, gif}',
                    dest: 'dist/'
                }]
            }
        },
        
        copy:{//pasa html 
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: '*.html',
                    dest: './dist'
                }]
            },
            fonts:{
                files: [{
                    
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/open-iconic/font',
                    src: ['fonts/*.*'],
                    dest: './dist'
                }]
            }
        },
        
        clean:{//elimina la carpeta 
            build:{
                src: ['./dist/']
            }
        },
        
        cssmin:{
            dist: {
                files:{
                    'dist/css/index.css':['css/*.css']
                }
            }
                
        },
        
        uglify:{// 
            dist: {
                files:{
                    'dist/js/main.js':['js/*.js']
                }
            }
        },
        
        filerev:{//pasa html 
            options: {
                enconding: 'utf8',
                algorithm: 'md5',
                lenght: 20
            },
            release: {
                files: [{                    
                    src: [
                        './dist/js/*.js',
                        './dist/css/*.css'
                    ]
                }]
            }
        },
        
        concat:{//
            options: {
                separator: ';'
            },
            dist: {}
        },
        
        useminPrepare:{
            foo: {
                dest: 'dist',
                src: ['app/index.html', 'app/about.html', 'app/precios.html', 'app/precios_tab-pill.html', 'app/contacto.html']
            },
            options:{
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function(context, block){
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }

                }
            }
            
        },
        
        usemin:{
            html: ['dist/index.html', 'dist/about.html', 'dist/precios.html', 'dist/precios_tab-pill.html', 'dist/contacto.html', 'dist'],
            options: {
                assetsDir: ['dist', 'dist/css', 'dist/js']
            }
        }
    });
    //con el require no hace faltan definir - antes estaban los load
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);

    grunt.registerTask('build', [
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
    ]);
};
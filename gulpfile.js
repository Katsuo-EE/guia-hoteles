'use strict'
var sass = require('gulp-sass')(require('sass'));
var gulp = require('gulp');
var browserSync = require('browser-sync');

var del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');

//configuracion de la tarea gulp llamada sass
gulp.task('sass', async function(){
    gulp.src('css/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

//habilitamos watch auto para generar los css 
gulp.task('sass:watch', function(){
    gulp.watch('css/**/*.scss', gulp.series('sass'));
});

gulp.task('browser-sync', function(){
    var files =  ['./*.html', './css/*.css', './img/*.{png, jpg, gif}', './js/*.js'];
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

gulp.task('default', gulp.parallel('browser-sync', 'sass:watch'));

//tarea de borrado
gulp.task('clean', function(){
    return del(['dist']);
});


gulp.task('copyfonts', async function(){
    gulp.src('./node_modules/open-iconic/font/fonts/**/*')
    .pipe(gulp.dest('./dist/fonts'));
});

// para comprimir img
gulp.task('imagemin', async function(){
    return gulp.src('./img/*')
        .pipe(imagemin({
            optimizationLevel: 7,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest('dist/img'));
});

// 
gulp.task('usemin', function(){
    return gulp.src('./*.html')
        .pipe(flatmap(function(stream, file){
            return stream
                .pipe(usemin({
                    css: [rev()],
                    html: [function(){
                        return htmlmin({
                            collapseWhitespace: true
                        })
                    }],
                    js: [uglify(), rev()],//para que se genere codigo de version
                    inlinejs: [uglify()],
                    inlinecss: [cleanCss(), 'concat']
                }))
        }))
        .pipe(gulp.dest('dist/')); // para que se escriba en la carpeta de distribucion
})

gulp.task('build', gulp.series('clean', gulp.parallel('copyfonts', 'imagemin', 'usemin')));



